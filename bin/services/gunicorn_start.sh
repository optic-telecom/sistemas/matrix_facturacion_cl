#!/bin/bash

NAME="Servidor_matrix_facturacion" # Name of the application
DJANGODIR=/home/facturacion/facturacion_backend/facturacioncl # Django project directory
LOGFILE=/home/facturacion/facturacion_backend/logs/gunicorn_facturacion.log
LOGDIR=$(dirname $LOGFILE)
#SOCKFILE=/home/facturacion/facturacion_backend/facturacion/config/gunicorn.sock # we will communicate using this unix socket
LOGDIR_PROJECT=/home/facturacion/facturacion_backend/logs # we will communicate using this unix socket
USER=facturacion # the user to run as
GROUP=facturacion # the group to run as
NUM_WORKERS=3 # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=config.settings.$1 # which settings file should Django use
DJANGO_WSGI_MODULE=config.wsgi
TIMEOUT=600

echo "Starting $NAME as `whoami`"

export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# # Create the run directory if it doesn't exist
# RUNDIR=$(dirname $SOCKFILE)
# test -d $RUNDIR || mkdir -p $RUNDIR
# Create the run directory if it doesn't exist for logs sentinel
test -d $LOGDIR_PROJECT || mkdir -p $LOGDIR_PROJECT

# Activate enviroment
source /home/facturacion/facturacion_env/bin/activate

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
#--bind=unix:$SOCKFILE \
exec /home/facturacion/facturacion_env/bin/gunicorn  ${DJANGO_WSGI_MODULE}:application \
--name $NAME \
--workers $NUM_WORKERS \
--user=$USER --group=$GROUP \
--bind=0.0.0.0:8080 \
--log-level=debug \
--timeout $TIMEOUT \
--log-file=$LOGFILE 2>>$LOGFILE
