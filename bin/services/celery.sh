#!/bin/bash
LOGFILE=/home/facturacion/facturacion_backend/logs/celery.log
LOGDIR=$(dirname $LOGFILE)
source /home/facturacion/facturacion_env/bin/activate
cd /home/facturacion/facturacion_backend/facturacioncl
exec celery worker -A tasks -B -l WARNING --logfile=$LOGFILE 2>>$LOGFILE
