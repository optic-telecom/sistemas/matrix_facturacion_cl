import json
import base64
from json2xml import json2xml
from json2xml.utils import readfromurl, readfromstring, readfromjson
from zeep import Client

client = Client("http://ws.facturacion.cl/WSDS/wsplano.asmx?wsdl")

with open("getTicket.json") as file:
    data = json.load(file)

for i in data["query"]:
    ticket = "ticket@" + i["tipo"] + "@" + i["folio"]
    attribs = {
        "login": {
            "Usuario": base64.b64encode(bytes(data["user"], "UTF-8")),
            "Rut": base64.b64encode(bytes(data["rut"], "UTF-8")),
            "Clave": base64.b64encode(bytes(data["password"], "UTF-8")),
            "Puerto": base64.b64encode(bytes(data["puerto"], "UTF-8")),
        },
        "ticket": base64.b64encode(bytes(ticket, "UTF-8")),
    }

    response = client.service.getBoletaTicket(**attribs)
    print(response)
