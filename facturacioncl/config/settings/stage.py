from .base import *  # noqa
from os.path import dirname, join
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from multifiberpy.sentry_multifiber import send_slack_event
"""
sentry_sdk.init(
    dsn="http://ce3f234adb494369acf89317621e6a06@190.113.247.197:9000/16",
    integrations=[DjangoIntegration()],
    before_send=send_slack_event,
)
"""

ENVIROMENT = "STAGE"
DEBUG = True
ALLOWED_HOSTS = ["*"]

"""
# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "facturacion",
        "USER": "facturacion_db",
        "PASSWORD": "RkAvc5DW+n/P@W~/",
        "HOST": "localhost",
    }
}

STATIC_ROOT = "/home/facturacion/facturacion_backend/static/"
SECRET_KEY = "9YjekzLZW5Dg7<QJ"
"""