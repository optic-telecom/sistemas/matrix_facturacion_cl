from .base import *  # noqa
from os.path import dirname, join
import json
import urllib3
from django.conf import settings

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from multifiberpy.sentry_multifiber import send_slack_event

ENVIROMENT = "PROD"
DEBUG = True
ALLOWED_HOSTS = ["*"]

"""
sentry_sdk.init(
    dsn="http://6c3510aa15404e65a538c17ca4206f80@190.113.247.197:9000/15",
    integrations=[DjangoIntegration()],
    before_send=send_slack_event,
)


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "facturacion",
        "USER": "facturacion_db",
        "PASSWORD": "Eapxuy9Sp9TEVy",
        "HOST": "localhost",
    }
}


SECRET_KEY = "9YjekzLZW5Dg7<QJ"
"""

STATIC_ROOT = "/home/facturacion/facturacion_backend/static/"
MEDIA_ROOT = "/home/facturacion/facturacion_backend/media/"

BASE_DIR_LOG = "/home/facturacion/facturacion_backend/logs/"

LOGGING = {
    "version": 1,
    "formatters": {"verbose": {"format": "%(levelname)s:%(name)s: %(message)s"}},
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
        "file": {
            "level": "INFO",
            "class": "logging.FileHandler",
            "formatter": "verbose",
            "filename": join(BASE_DIR_LOG, "debug_django.log"),
        },
    },
    "loggers": {
        "django.request": {
            "handlers": ["file", "console"],
            "level": "DEBUG",
            "propagate": False,
        },
        "": {"level": "DEBUG", "handlers": ["console"],},
    },
}
