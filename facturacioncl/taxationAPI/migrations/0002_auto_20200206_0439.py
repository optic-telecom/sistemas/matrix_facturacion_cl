# Generated by Django 2.2.5 on 2020-02-06 04:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('taxationAPI', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='taxationmodel',
            name='password',
            field=models.CharField(default='plano91098', max_length=20),
        ),
        migrations.AddField(
            model_name='taxationmodel',
            name='puerto',
            field=models.CharField(default='10177', max_length=10),
        ),
        migrations.AddField(
            model_name='taxationmodel',
            name='rut',
            field=models.CharField(default='1-9', max_length=12),
        ),
        migrations.AddField(
            model_name='taxationmodel',
            name='user',
            field=models.CharField(default='BANDAANCHA', max_length=30),
        ),
    ]
