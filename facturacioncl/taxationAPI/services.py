import requests, json, ssl


def generateRequest(params, url):
    send = requests.post(
        url,
        headers={"Content-Type": "application/json"},
        data=params,
        verify=ssl.CERT_NONE,
        timeout=120,
    )
    print(send.content)
    return send
