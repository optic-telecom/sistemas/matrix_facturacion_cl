import xmltodict, json, base64, zeep, subprocess, os, datetime, shlex
from taxationAPI.models import *
from taxationAPI.services import *
from lxml import etree
from django.conf import settings
from tasks.celery import app as celery_app
from os import remove
from zeep import Client
from zeep.transports import Transport
from json2xml import json2xml
from json2xml.utils import readfromurl, readfromstring, readfromjson
from requests.exceptions import ConnectionError
from celery import Celery
from celery.schedules import crontab


@celery_app.task(name="barcode")
def periodicBarcode():
    print("Iniciando tarea: Pedir Barcode.")

    client = Client(
        "http://ws.facturacion.cl/WSDS/wsplano.asmx?wsdl",
        transport=Transport(timeout=120),
    )

    documents = documentCode.objects.filter(done=False)

    if documents:
        for doc in documents:
            login = doc.access
            attribs = {
                "login": {
                    "Usuario": base64.b64encode(bytes(login["Usuario"], "UTF-8")),
                    "Rut": base64.b64encode(bytes(login["Rut"], "UTF-8")),
                    "Clave": base64.b64encode(bytes(login["Clave"], "UTF-8")),
                    "Puerto": base64.b64encode(bytes(login["Puerto"], "UTF-8")),
                },
                "ticket": base64.b64encode(bytes(doc.ticket, "UTF-8")),
            }

            reply = client.service.getBoletaTicket(**attribs)
            result = xmltodict.parse(reply)

            # Barcode key exists then success
            try:
                response = result["WSPLANO"]["Mensaje"]["TED"]
                documentCode.objects.filter(pk=doc.pk).update(reply=response, done=True)
                status = "Barcode generado"

                # Send it back to matrix
                data = {
                    "response": {
                        "folio": doc.ticket.split("@")[2],
                        "kind": doc.ticket.split("@")[1],
                        "barCode": response,
                        "addBarCode": True,
                    },
                    "order": False,
                }
                print("data", data)
                send = generateRequest(json.dumps(data), doc.endPoint)
                print("barcode is sended", send)
                documentCode.objects.filter(id=doc.id).update(done=True)

                # Barcode doesn't ready, maybe in another moment
            except KeyError:
                try:
                    response = result["WSPLANO"]["Mensaje"]["Error"]
                    status = "Error en barcode"

                except KeyError:
                    status = "Error desconocido"
            print(status, doc.ticket)


@celery_app.task(name="tax")
def tax(data={}):
    # TAXATION
    client = Client(
        "http://ws.facturacion.cl/WSDS/wsplano.asmx?wsdl",
        transport=Transport(timeout=120),
    )
    # Get only documents from data
    documentList = data["documentList"]
    # Mapping to separate documents
    result = []  # List for dump into JSON obj
    invoiceData = {}
    celery = data["celery"]
    for i in documentList:
        # Invoice data
        invData = {
            i["Documento"]["Encabezado"]["IdDoc"]["Folio"]: {
                "operatorRut": i["Documento"]["Encabezado"]["Emisor"]["RUTEmisor"],
                "totalAmount": i["Documento"]["Encabezado"]["Totales"]["MntTotal"],
            }
        }
        invoiceData.update(invData)
        # Get JSON object from a Python dict
        dictionaryToJson = json.dumps(i)

        # Get the XML from a JSON string
        readJson = readfromstring(dictionaryToJson)

        # Get the XML string
        documentxml = json2xml.Json2xml(readJson, wrapper="DTE", pretty=False).to_xml()
        if data["unified"]:
            a = documentxml
            a = a.decode("utf-8")
            a = a.replace('<Detalle type="list">', "")
            a = a.replace("</Detalle>", "")
            a = a.replace(
                '<item type="dict"><NroLinDet', '<Detalle type="dict"><NroLinDet'
            )
            a = a.replace("</MontoItem></item>", "</MontoItem></Detalle>")
            a = a.replace("</QtyItem></item>", "</QtyItem></Detalle>")
            a = a.replace('<DscRcgGlobal type="list">', "")
            a = a.replace("</DscRcgGlobal>", "")
            a = a.replace(
                '<item type="dict"><NroLinDR', '<DscRcgGlobal type="dict"><NroLinDR'
            )
            a = a.replace("</TpoValor></item>", "</TpoValor></DscRcgGlobal>")
            documentxml = bytes(a, "utf-8")
        else:
            try:
                if type(i["Documento"]["DscRcgGlobal"]) is list:
                    a = documentxml
                    a = a.decode("utf-8")
                    a = a.replace('<DscRcgGlobal type="list">', "")
                    a = a.replace("</DscRcgGlobal>", "")
                    a = a.replace(
                        '<item type="dict"><NroLinDR',
                        '<DscRcgGlobal type="dict"><NroLinDR',
                    )
                    a = a.replace("</TpoValor></item>", "</TpoValor></DscRcgGlobal>")
                    documentxml = bytes(a, "utf-8")
            except:
                pass
        print(documentxml)
        attribs = {
            "login": {
                "Usuario": base64.b64encode(bytes(data["user"], "UTF-8")),
                "Rut": base64.b64encode(bytes(data["rut"], "UTF-8")),
                "Clave": base64.b64encode(bytes(data["password"], "UTF-8")),
                "Puerto": base64.b64encode(bytes(data["puerto"], "UTF-8")),
            },
            "file": base64.encodebytes(documentxml),
            "formato": "2",
        }

        ##### Requesting FacturacionWS #####
        try:
            responseXML = client.service.Procesar(**attribs)
            print("Envío de responseXML exitoso")
        except ConnectionError:
            print("Error al enviar el responseXML")
            log = {
                "folio": i["Documento"]["Encabezado"]["IdDoc"]["Folio"],
                "tipoDte": i["Documento"]["Encabezado"]["IdDoc"]["TipoDte"],
                "resultado": "False",
                "error": "El documento no se procesó. No se estableció una conexión con facturacion.cl",
                "invoiceData": invData,
            }
            result.append(log)
            continue
        # Pack the data
        doc = xmltodict.parse(responseXML)
        log = {
            "folio": doc["WSPLANO"]["Detalle"]["Documento"]["Folio"],
            "tipoDte": doc["WSPLANO"]["Detalle"]["Documento"]["TipoDte"],
            "resultado": doc["WSPLANO"]["Detalle"]["Documento"]["Resultado"],
        }

        if log["resultado"] == "False":
            log["error"] = doc["WSPLANO"]["Detalle"]["Documento"]["Error"]
        else:
            log["error"] = False
            log["barCode"] = "Aún no emitido."
            log["barCodeStatus"] = False

            # Create document request for bar codes.
            documentBarCode = {
                "access": {
                    "Usuario": data["user"],
                    "Rut": data["rut"],
                    "Clave": data["password"],
                    "Puerto": data["puerto"],
                },
                "ticket": "ticket@" + str(log["tipoDte"]) + "@" + str(log["folio"]),
                "endPoint": data["endPoint"],
            }

            documentCode.objects.create(
                access=documentBarCode["access"],
                ticket=documentBarCode["ticket"],
                endPoint=documentBarCode["endPoint"],
            )

            ########## ##########
            """ ticket = "ticket@" + str(log["TipoDte"]) + "@" + str(log["Folio"])
            attribs = {
                "login": {
                    "Usuario": base64.b64encode(bytes(data["user"], "UTF-8")),
                    "Rut": base64.b64encode(bytes(data["rut"], "UTF-8")),
                    "Clave": base64.b64encode(bytes(data["password"], "UTF-8")),
                    "Puerto": base64.b64encode(bytes(data["puerto"], "UTF-8")),
                },
                "ticket": base64.b64encode(bytes(ticket, "UTF-8")),
            }
            bar = periodicBarcode(attribs)
            print("bar", bar) """
            ########## ##########
        if celery:
            log["create_invoice"] = True
        else:
            log["create_invoice"] = False

        # Add invoiceData to log
        log["invoiceData"] = invData
        # Save log dict into result list
        result.append(log)
    reuqestedData = {
        "response": result,
        "order": data["order"],
        "invoiceData": invoiceData,
    }
    # Pretty printing the JSON obj
    print(json.dumps(reuqestedData, indent=4, sort_keys=True))
    ResponseModel.objects.create(documentList=result, endPoint=data["endPoint"])
    createResponse = generateRequest(json.dumps(reuqestedData), data["endPoint"])
    print(createResponse.json())
    return result


def notes(data={}):
    try:
        # TAXATION
        client = Client(
            "http://ws.facturacion.cl/WSDS/wsplano.asmx?wsdl",
            transport=Transport(timeout=120),
        )
        # Get only documents from data
        documentList = data["documentList"]
        # Mapping to separate documents
        result = []  # List for dump into JSON obj
        invoiceData = {}
        celery = data["celery"]
        for i in documentList:
            # Invoice data
            invData = {
                i["Documento"]["Encabezado"]["IdDoc"]["Folio"]: {
                    "operatorRut": i["Documento"]["Encabezado"]["Emisor"]["RUTEmisor"],
                    "totalAmount": i["Documento"]["Encabezado"]["Totales"]["MntTotal"],
                }
            }
            invoiceData.update(invData)
            # Get JSON object from a Python dict
            dictionaryToJson = json.dumps(i)

            # Get the XML from a JSON string
            readJson = readfromstring(dictionaryToJson)

            # Get the XML string
            documentxml = json2xml.Json2xml(
                readJson, wrapper="DTE", pretty=False
            ).to_xml()
            a = documentxml
            a = a.decode("utf-8")
            a = a.replace('<Detalle type="list">', "")
            a = a.replace("</Detalle>", "")
            a = a.replace(
                '<item type="dict"><NroLinDet', '<Detalle type="dict"><NroLinDet'
            )
            a = a.replace("</MontoItem></item>", "</MontoItem></Detalle>")
            documentxml = bytes(a, "utf-8")
            attribs = {
                "login": {
                    "Usuario": base64.b64encode(bytes(data["user"], "UTF-8")),
                    "Rut": base64.b64encode(bytes(data["rut"], "UTF-8")),
                    "Clave": base64.b64encode(bytes(data["password"], "UTF-8")),
                    "Puerto": base64.b64encode(bytes(data["puerto"], "UTF-8")),
                },
                "file": base64.encodebytes(documentxml),
                "formato": "2",
            }

            # Calling method Procesar
            responseXML = client.service.Procesar(**attribs)

            # Pack the data
            doc = xmltodict.parse(responseXML)
            log = {
                "folio": doc["WSPLANO"]["Detalle"]["Documento"]["Folio"],
                "tipoDte": doc["WSPLANO"]["Detalle"]["Documento"]["TipoDte"],
                "resultado": doc["WSPLANO"]["Detalle"]["Documento"]["Resultado"],
            }
            if log["resultado"] == "False":
                log["error"] = doc["WSPLANO"]["Detalle"]["Documento"]["Error"]
            else:
                log["error"] = False

            if celery:
                log["create_invoice"] = True
            else:
                log["create_invoice"] = False
            # Adding BARCODE
            ticket = (
                "ticket@"
                + str(i["Documento"]["Encabezado"]["IdDoc"]["TipoDTE"])
                + "@"
                + str(i["Documento"]["Encabezado"]["IdDoc"]["Folio"])
            )
            attribs = {
                "login": {
                    "Usuario": base64.b64encode(bytes(data["user"], "UTF-8")),
                    "Rut": base64.b64encode(bytes(data["rut"], "UTF-8")),
                    "Clave": base64.b64encode(bytes(data["password"], "UTF-8")),
                    "Puerto": base64.b64encode(bytes(data["puerto"], "UTF-8")),
                },
                "ticket": base64.b64encode(bytes(ticket, "UTF-8")),
            }
            # Calling method getBoletaTicket
            response = client.service.getBoletaTicket(**attribs)
            # Handling data
            doc = xmltodict.parse(response)
            try:
                log["barCode"] = doc["WSPLANO"]["Mensaje"]["TED"]
            except KeyError:
                try:
                    log["barCode"] = doc["WSPLANO"]["Mensaje"]["Error"]
                except KeyError:
                    log["barCode"] = "Error desconocido"
            # Save log dict into result list
            result.append(log)
        reuqestedData = {
            "response": result,
            "invoiceData": invoiceData,
        }
        print(json.dumps(result, indent=4, sort_keys=True))
        return json.dumps(result)

    except ConnectionError as error:
        return "Error de conexión"
