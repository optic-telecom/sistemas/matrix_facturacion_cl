from django.db import models
from datetime import datetime
from django.contrib.postgres.fields import JSONField
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone


class DocumentModel(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField(default=timezone.now(), blank=True)
    user = models.CharField(max_length=30, blank=False, default="BANDAANCHA")
    rut = models.CharField(max_length=12, blank=False, default="1-9")
    password = models.CharField(max_length=20, blank=False, default="plano91098")
    puerto = models.CharField(max_length=10, blank=False, default="10177")
    documentList = JSONField(null=True, blank=False)


class ResponseModel(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField(default=timezone.now(), blank=True)
    documentList = JSONField(null=True, blank=False)
    endPoint = models.CharField(null=True, blank=False, max_length=100)

    def __str__(self):
        return self.documentList

    class Meta:
        ordering = ["-date"]


class ExtraDataModel(models.Model):
    id = models.AutoField(primary_key=True)
    receptor = models.IntegerField(null=True, blank=False)
    date = models.DateTimeField(default=timezone.now(), blank=False)
    message = JSONField(null=True, blank=False)
    endPoint = models.CharField(null=True, blank=False, max_length=200)

    def __str__(self):
        return str(id)

    class Meta:
        ordering = ["-date"]


class documentCode(models.Model):
    id = models.AutoField(primary_key=True)
    added = models.DateTimeField(_("added"), auto_now_add=True)
    ticket = models.CharField(_("ticket"), max_length=50, null=False, blank=False)
    access = JSONField(_("access"), null=False, blank=False)
    endPoint = models.CharField(_("end_point"), null=True, blank=False, max_length=200)
    reply = JSONField(_("reply"), null=True, blank=True)
    done = models.BooleanField(_("done"), default=False)

    def __str__(self):
        return ticket

    class Meta:
        ordering = ["-added"]
