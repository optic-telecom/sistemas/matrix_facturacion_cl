from rest_framework import viewsets
from taxationAPI.serializers import *
from taxationAPI.models import *


class DocumentViewSet(viewsets.ModelViewSet):
    model = DocumentModel
    queryset = DocumentModel.objects.all()
    serializer_class = DocumentSerializer


class ExtraDataViewSet(viewsets.ModelViewSet):
    model = ExtraDataModel
    queryset = ExtraDataModel.objects.all()
    serializer_class = ExtraDataSerializer

