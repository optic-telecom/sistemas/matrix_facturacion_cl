from django.apps import AppConfig


class TaxationapiConfig(AppConfig):
    name = 'taxationAPI'
