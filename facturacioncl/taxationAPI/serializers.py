from django.contrib.postgres.fields import JSONField
from rest_framework import serializers
from rest_framework.response import Response
from taxationAPI.models import *
from taxationAPI.tasks import *
from django.utils import timezone


class DocumentSerializer(serializers.Serializer):

    user = serializers.CharField(max_length=30)
    rut = serializers.CharField(max_length=12)
    password = serializers.CharField(max_length=20)
    puerto = serializers.CharField(max_length=10)
    documentList = serializers.JSONField()
    celery = serializers.BooleanField(write_only=True)
    endPoint = serializers.CharField(max_length=100, write_only=True)
    order = serializers.JSONField(write_only=True)
    unified = serializers.BooleanField(write_only=True)

    def create(self, validated_data):
        dataToModel = {
            "user": validated_data["user"],
            "rut": validated_data["rut"],
            "password": validated_data["password"],
            "puerto": validated_data["puerto"],
            "documentList": validated_data["documentList"],
        }
        DocumentModel.objects.create(**dataToModel)
        if validated_data["celery"]:
            tax.delay(validated_data)
            return validated_data
        else:
            return {
                "user": False,
                "rut": False,
                "password": False,
                "puerto": False,
                "documentList": notes(validated_data),
                "celery": False,
                "endPoint": False,
                "order": False,
                "unified": False,
            }
            # return validated_data

    class Meta:
        model = DocumentModel
        fields = "__all__"


class ExtraDataSerializer(serializers.Serializer):
    receptor = models.IntegerField()
    date = models.DateTimeField(default=timezone.now())
    message = JSONField()
    endPoint = models.CharField(max_length=200)

    def create(self, validated_data):
        data = self.initial_data
        try:
            date = data["date"]
        except:
            date = timezone.now
        ExtraDataModel.objects.create(
            receptor=data["receptor"],
            date=date,
            message=data["message"],
            endPoint=data["endPoint"],
        )
        return json.loads(data)

    class Meta:
        model = ExtraDataModel
        fields = "__all__"
