from django.contrib import admin
from .models import *
from simple_history.admin import SimpleHistoryAdmin
from import_export.admin import ImportExportModelAdmin

# Register your models here.
class ResponseModelAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = (
        "id",
        "date",
        "documentList",
    )


admin.site.register(ResponseModel, ResponseModelAdmin)


class DocumentModelAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = (
        "id",
        "date",
        "documentList",
    )


admin.site.register(DocumentModel, DocumentModelAdmin)
