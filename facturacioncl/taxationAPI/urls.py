from django.urls import include, path
from rest_framework import routers
from taxationAPI import views

router = routers.DefaultRouter()
router.register(r"document", views.DocumentViewSet)
router.register(r"extradata", views.ExtraDataViewSet)

urlpatterns = [
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    path("", include(router.urls)),
]
