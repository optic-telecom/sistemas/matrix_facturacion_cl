from django.urls import path, re_path
from rest_framework import routers
#from common.views import *

router = routers.DefaultRouter()

def trigger_error(request):
    division_by_zero = 1 / 0

urlpatterns = [
    path('sentry-debug/', trigger_error, name='sentry-debug'),
]