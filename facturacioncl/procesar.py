import xmltodict
import json
import base64
from lxml import etree
from json2xml import json2xml
from json2xml.utils import readfromurl, readfromstring, readfromjson
from zeep import Client

with open("procesar.json") as file:
    data = json.load(file)

# TAXATION
client = Client("http://ws.facturacion.cl/WSDS/wsplano.asmx?wsdl")

# Get only documents from data
documentList = data["documentList"]

# Mapping to separate documents
result = {}  # Dict for dump into JSON obj
a = 1
for i in documentList:
    # Get JSON object from a Python dict
    dictionaryToJson = json.dumps(i)

    # Get the XML from a JSON string
    readJson = readfromstring(dictionaryToJson)

    # Get the XML string
    # documentxml = json2xml.Json2xml(readJson, wrapper="DTE", pretty=False).to_xml()
    documentxml = '<?xml version="1.0" encoding="UTF-8"?><DTE><Documento type="dict"><Encabezado type="dict"><IdDoc type="dict"><TipoDTE type="int">39</TipoDTE><Folio type="int">497048</Folio><FchEmis type="str">2020-07-22</FchEmis><IndServicio type="int">3</IndServicio><IndMntNeto type="int">2</IndMntNeto><FchVenc type="str">2020-08-05</FchVenc></IdDoc><Emisor type="dict"><RUTEmisor type="str">76171094-k</RUTEmisor><RznSocEmisor type="str">OPTIC TELECOMUNICACIONES LTDA.</RznSocEmisor><GiroEmisor type="str">Servicio de telecomunicaciones e instalaci\xc3\xb3n de redes.</GiroEmisor><DirOrigen type="str">ANTONIA LOPEZ DE BELLO 114 OF. 403</DirOrigen><CmnaOrigen type="str">RECOLETA</CmnaOrigen><CiudadOrigen type="str">SANTIAGO</CiudadOrigen></Emisor><Receptor type="dict"><RUTRecep type="str">17011320-9</RUTRecep><CdgIntRecep type="int">12550</CdgIntRecep><RznSocRecep type="str">Jessica Alejandra Retamal Guerrero</RznSocRecep><Contacto type="str">Jesssicaretamalguerrero@gmail.com</Contacto><DirRecep type="str">Lionel Valcarce, 15101, Torre 29, Apto. 202, Casa Nro.110</DirRecep><CmnaRecep type="str">Arica</CmnaRecep><CiudadRecep type="str">Arica</CiudadRecep><GiroRecep type="str">No espec\xc3\xadfico</GiroRecep></Receptor><Totales type="dict"><MntNeto type="int">9000</MntNeto><MntExe type="int">0</MntExe><IVA type="int">1710</IVA><MntTotal type="int">10710</MntTotal><SaldoAnterior type="int">0</SaldoAnterior><MontoNF type="int">0</MontoNF><TotalPeriodo type="int">10710</TotalPeriodo><VlrPagar type="int">10710</VlrPagar></Totales></Encabezado><Detalle type="dict"><NroLinDet type="int">1</NroLinDet><CdgItem type="dict"><TpoCodigo type="str">INT1</TpoCodigo><VlrCodigo type="int">113</VlrCodigo></CdgItem><NmbItem type="str">2018 - Fibra - Internet 60Megas</NmbItem><QtyItem type="int">1</QtyItem><UnmdItem type="str">UN</UnmdItem><PrcItem type="float">10000</PrcItem><MontoItem type="int">10000</MontoItem></Detalle><DscRcgGlobal type="dict"><NroLinDR type="int">1</NroLinDR><TpoMov type="str">D</TpoMov><GlosaDR type="str">descuento prueba %</GlosaDR><ValorDR type="int">1000</ValorDR><IndExeDR type="int">0</IndExeDR><TpoValor type="str">%</TpoValor></DscRcgGlobal></Documento></DTE>'

    attribs = {
        "login": {
            "Usuario": base64.b64encode(bytes(data["user"], "UTF-8")),
            "Rut": base64.b64encode(bytes(data["rut"], "UTF-8")),
            "Clave": base64.b64encode(bytes(data["password"], "UTF-8")),
            "Puerto": base64.b64encode(bytes(data["puerto"], "UTF-8")),
        },
        "file": base64.b64encode(bytes(documentxml, "UTF-8")),
        "formato": "2",
    }

    # Calling method Procesar
    responseXML = client.service.Procesar(**attribs)
    """ root = etree.fromstring(responseXML)
    print(etree.tostring(root, pretty_print=True).decode(), "\n") """

    # Pack the data
    doc = xmltodict.parse(responseXML)
    folio = doc["WSPLANO"]["Detalle"]["Documento"]["Folio"]
    tipoDte = doc["WSPLANO"]["Detalle"]["Documento"]["TipoDte"]
    resultado = doc["WSPLANO"]["Detalle"]["Documento"]["Resultado"]
    if resultado == "False":
        error = doc["WSPLANO"]["Detalle"]["Documento"]["Error"]
    else:
        error = None

    response = {
        "folio": folio,
        "tipoDte": tipoDte,
        "resultado": resultado,
        "error": error,
    }

    # Save response data into result dict
    result[a] = response
    a += 1
    # print("\n\n")

# PDF417
a = 1
for i in documentList:
    TipoDTE = i["Documento"]["Encabezado"]["IdDoc"]["TipoDTE"]
    Folio = i["Documento"]["Encabezado"]["IdDoc"]["Folio"]
    ticket = "ticket@" + TipoDTE + "@" + Folio
    attribs = {
        "login": {
            "Usuario": base64.b64encode(bytes(data["user"], "UTF-8")),
            "Rut": base64.b64encode(bytes(data["rut"], "UTF-8")),
            "Clave": base64.b64encode(bytes(data["password"], "UTF-8")),
            "Puerto": base64.b64encode(bytes(data["puerto"], "UTF-8")),
        },
        "ticket": base64.b64encode(bytes(ticket, "UTF-8")),
    }
    # Calling method getBoletaTicket
    response = client.service.getBoletaTicket(**attribs)
    """ root = etree.fromstring(response)
    print(etree.tostring(root, pretty_print=True).decode()) """
    # Handling data
    doc = xmltodict.parse(response)
    try:
        barCode = doc["WSPLANO"]["Mensaje"]["TED"]
    except KeyError:
        try:
            barCode = doc["WSPLANO"]["Mensaje"]["Error"]
        except KeyError:
            barCode = "Error desconocido"
    # Save PDF417 code into result dict
    result[a]["barCode"] = barCode
    a += 1
    # print("\n\n")

# Dump result data into JSON object
responseJson = json.dumps(result)

# Pretty printing the JSON obj
print(json.dumps(result, indent=4, sort_keys=True))
