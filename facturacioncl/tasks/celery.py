from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from django.conf import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.local")

app = Celery("tasks")

app.config_from_object("django.conf:settings", namespace="CELERY")

app.conf.beat_schedule = {"every-hour": {"task": "barcode", "schedule": 60,}}

app.autodiscover_tasks(settings.INSTALLED_APPS)
